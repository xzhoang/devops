
echo 'deploy app to server PROD: =======>'
rm -fr $HOME/pythonapp
mkdir -p $HOME/pythonapp
cd $HOME/pythonapp
git clone https://gitlab.com/xzhoang/devops.git .
docker stack deploy --compose-file docker-compose-prod.yml stackpython
echo '=====> deploy success on PROD server'
